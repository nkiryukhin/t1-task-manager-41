package ru.t1.nkiryukhin.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.ISessionRepository;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.ISessionService;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;

import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Nullable
    @Override
    public SessionDTO add(@Nullable final SessionDTO session) {
        if (session == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractFieldException {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAll(userId);
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
             @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
             return sessionRepository.findOneById(id);
        }
    }

    @Override
    public int getSize() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.getSize();
        }
    }

    @Override
    public void remove(final List<SessionDTO> sessions) {
        for (final SessionDTO session : sessions) {
            removeOne(session);
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        SessionDTO session = findOneById(id);
        removeOne(session);
    }

    @Override
    public void removeOne(@Nullable final SessionDTO session) {
        if (session == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.remove(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        List<SessionDTO> sessions = findAll(userId);
        if (sessions == null || sessions.isEmpty()) return;
        remove(sessions);
    }

}

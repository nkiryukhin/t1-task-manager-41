package ru.t1.nkiryukhin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.repository.ITaskRepository;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.ITaskService;
import ru.t1.nkiryukhin.tm.comparator.CreatedComparator;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.comparator.StatusComparator;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO add(@Nullable final TaskDTO task) {
        if (task == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Override
    public TaskDTO add(@Nullable final String userId, @Nullable final TaskDTO task) throws UserIdEmptyException {
        if (userId == null) throw new UserIdEmptyException();
        if (task == null) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Override
    @SneakyThrows
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (status == null) throw new StatusEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO(name, description);
        task.setUserId(userId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAll(userId);
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (comparator == NameComparator.INSTANCE) return taskRepository.findAllOrderByName(userId);
            if (comparator == CreatedComparator.INSTANCE) return taskRepository.findAllOrderByCreated(userId);
            if (comparator == StatusComparator.INSTANCE) return taskRepository.findAllOrderByStatus(userId);
            return taskRepository.findAll(userId);
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOneById(userId, id);
        }
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOneByIndex(userId, index);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        List<TaskDTO> tasks;
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            tasks = taskRepository.findAllByProjectId(userId, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    @Override
    public int getSize(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.getSize(userId);
        }
    }

    @Override
    public void remove(@Nullable Collection<TaskDTO> tasks) throws AbstractException {
        if (tasks == null || tasks.isEmpty()) return;
        for (TaskDTO task : tasks) {
            removeOne(task);
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        TaskDTO task = findOneByIndex(userId, index);
        removeOne(task);
    }

    @Override
    public void removeOne(@Nullable final TaskDTO task) throws AbstractException {
        if (task == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.remove(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateProjectId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}

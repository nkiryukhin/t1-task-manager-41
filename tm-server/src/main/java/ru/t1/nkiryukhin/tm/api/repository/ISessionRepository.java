package ru.t1.nkiryukhin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, created, role, user_id)" +
            " VALUES (#{id}, #{created}, #{role}::role, #{userId})")
    void add(@NotNull SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results({
            @Result(property = "userId", column = "user_id"),
    })
    @Nullable List<SessionDTO> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE id = #{id} LIMIT 1")
    @Results({
            @Result(property = "userId", column = "user_id"),
    })
    @Nullable SessionDTO findOneById(@NotNull String id);

    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results({
            @Result(property = "userId", column = "user_id"),
    })
    @Nullable SessionDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull Integer index) throws AbstractFieldException;

    @Select("SELECT COUNT(1) FROM tm_session")
    int getSize();

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void remove(@Nullable SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

}
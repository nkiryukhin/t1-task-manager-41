package ru.t1.nkiryukhin.tm.data;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static ru.t1.nkiryukhin.tm.data.ProjectTestData.USUAL_PROJECT1;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static TaskDTO USUAL_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO USUAL_TASK2 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN_TASK2 = new TaskDTO();

    @Nullable
    public final static TaskDTO NULL_TASK = null;

    @NotNull
    public final static String NON_EXISTING_TASK_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<TaskDTO> USUAL_TASK_LIST = Arrays.asList(USUAL_TASK1, USUAL_TASK2);

    @NotNull
    public final static List<TaskDTO> ADMIN_TASK_LIST = Arrays.asList(ADMIN_TASK1, ADMIN_TASK2);

    @NotNull
    public final static List<TaskDTO> TASK_LIST = new ArrayList<>();

    @NotNull
    public final static List<TaskDTO> SORTED_TASK_LIST = new ArrayList<>();

    static {
        USUAL_TASK_LIST.forEach(task -> task.setUserId(UserTestData.USUAL_USER.getId()));
        USUAL_TASK1.setName("Usual Task 1");
        USUAL_TASK2.setName("Usual Task 2");
        USUAL_TASK1.setDescription("Usual Task 1 Desc");
        USUAL_TASK2.setDescription("Usual Task 2 Desc");
        USUAL_TASK_LIST.forEach(task -> task.setProjectId(USUAL_PROJECT1.getId()));
        ADMIN_TASK_LIST.forEach(task -> task.setUserId(UserTestData.ADMIN_USER.getId()));
        ADMIN_TASK1.setName("Admin Task 1");
        ADMIN_TASK2.setName("Admin Task 2");
        ADMIN_TASK1.setDescription("Admin Task 1 Desc");
        ADMIN_TASK2.setDescription("Admin Task 2 Desc");
        TASK_LIST.addAll(USUAL_TASK_LIST);
        TASK_LIST.addAll(ADMIN_TASK_LIST);
        SORTED_TASK_LIST.addAll(TASK_LIST);
        SORTED_TASK_LIST.sort(NameComparator.INSTANCE);
    }

}

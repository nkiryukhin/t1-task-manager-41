package ru.t1.nkiryukhin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.api.service.*;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;

import static ru.t1.nkiryukhin.tm.data.SessionTestData.*;
import static ru.t1.nkiryukhin.tm.data.UserTestData.ADMIN_USER;
import static ru.t1.nkiryukhin.tm.data.UserTestData.USUAL_USER;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull static final IPropertyService propertyService = new PropertyService();

    @NotNull static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull static final ITaskService taskService = new TaskService(connectionService);

    @NotNull static final ISessionService sessionService = new SessionService(connectionService);

    @NotNull static final IUserService userService = new UserService(connectionService, propertyService, taskService, projectService, sessionService);


    @BeforeClass
    public static void setUp() {
        userService.add(USUAL_USER);
        userService.add(ADMIN_USER);
    }

    @Before
    public void before() {
        sessionService.add(USUAL_SESSION1);
        sessionService.add(USUAL_SESSION2);
    }

    @After
    public void after() throws AbstractException {
        sessionService.remove(SESSION_LIST);
    }

    @AfterClass
    public static void tearDown() throws UserIdEmptyException {
        userService.removeOne(ADMIN_USER);
        userService.removeOne(USUAL_USER);
    }

    @Test
    public void add() throws AbstractFieldException {
        Assert.assertNull(sessionService.add(NULL_SESSION));
        Assert.assertNotNull(sessionService.add(ADMIN_SESSION1));
        @Nullable final SessionDTO session = sessionService.findOneById(ADMIN_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION1, session);
    }

    @Test
    public void existsById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            sessionService.existsById("");
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            sessionService.existsById(null);
        });
        Assert.assertFalse(sessionService.existsById(NON_EXISTING_SESSION_ID));
        Assert.assertTrue(sessionService.existsById(USUAL_SESSION1.getId()));
    }

    @Test
    public void findAll() throws UserIdEmptyException {
        Assert.assertEquals(USER_SESSION_LIST, sessionService.findAll(USUAL_USER.getId()));
    }

    @Test
    public void findOneById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            sessionService.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            sessionService.findOneById("");
        });
        Assert.assertNull(sessionService.findOneById(NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = sessionService.findOneById(USUAL_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USUAL_SESSION1, session);
    }

    @Test
    public void getSize() {
        int initCount = sessionService.getSize();
        sessionService.add(ADMIN_SESSION1);
        Assert.assertEquals(initCount + 1, sessionService.getSize());
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final SessionDTO createdSession = sessionService.add(ADMIN_SESSION1);
        Assert.assertTrue(sessionService.existsById(ADMIN_SESSION1.getId()));
        sessionService.removeOne(createdSession);
        Assert.assertFalse(sessionService.existsById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void removeById() throws AbstractFieldException {
        Assert.assertThrows(IdEmptyException.class, () -> {
            sessionService.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            sessionService.removeById("");
        });
        sessionService.add(ADMIN_SESSION1);
        Assert.assertTrue(sessionService.existsById(ADMIN_SESSION1.getId()));
        sessionService.removeById(ADMIN_SESSION1.getId());
        Assert.assertFalse(sessionService.existsById(ADMIN_SESSION1.getId()));
    }

    @Test
    public void remove() throws AccessDeniedException, UserIdEmptyException {
        int count = sessionService.getSize();
        sessionService.remove(SESSION_LIST);
        Assert.assertEquals(count - 2, sessionService.getSize());
    }

    @Test
    public void removeAllByUserId() throws AccessDeniedException, AbstractFieldException {
        sessionService.removeAllByUserId(USUAL_USER.getId());
        Assert.assertFalse(sessionService.existsById(USUAL_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USUAL_SESSION2.getId()));
    }

}


package ru.t1.nkiryukhin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.model.IWBS;
import ru.t1.nkiryukhin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private Date created = new Date();

    @Column
    @Nullable
    private String description = "";

    @Column
    @NotNull
    private String name = "";

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    public TaskDTO(@NotNull final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO(@NotNull final String name, @Nullable final String description, @NotNull final Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description + " : " + status.getDisplayName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskDTO task = (TaskDTO) o;
        return created.equals(task.created)
                && name.equals(task.name)
                && Objects.equals(projectId, task.projectId)
                && status == task.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, projectId);
    }

}

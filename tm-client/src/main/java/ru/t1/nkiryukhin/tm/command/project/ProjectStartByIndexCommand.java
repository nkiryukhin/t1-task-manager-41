package ru.t1.nkiryukhin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.ProjectStartByIndexRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Start project by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpoint().startProjectByIndex(request);
    }

}
